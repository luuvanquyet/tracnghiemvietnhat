﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebMasterPage.master" AutoEventWireup="true" CodeFile="web_TracNghiem.aspx.cs" Inherits="web_module_web_TracNghiem" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="higlobal" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hislider" runat="Server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibelowtop" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <style>
        .panel {
            margin-bottom: 18px;
            margin-bottom: 18px;
            background-color: #fff;
            border: 1px solid transparent;
            border-radius: 4px;
            -webkit-box-shadow: 0 1px 1px rgb(0 0 0 / 17%);
            box-shadow: 0 1px 1px rgb(0 0 0 / 17%);
        }

        .panel-body ul {
            margin: 0;
            padding: 0;
        }

        .exam-info {
            color: #428bca;
            font-size: 16px;
        }

        .list-inline {
            padding-left: 0;
            list-style: none;
            margin-left: -5px;
        }

            .list-inline > li {
                display: inline-block;
                padding-left: 5px;
                padding-right: 5px;
            }

        a:hover, a:focus {
            text-decoration: none;
            color: #c19d5e;
        }
    </style>
    <div class="container">
        <h4 class="event__title">Kiểm Tra Trắc Nghiệm</h4>
        <div class="content">
            <div class="test viewlist">
                <asp:Repeater ID="rpTracNghiem" runat="server">
                    <ItemTemplate>
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <h2>
                                    <a href="/bai-luyen-tap-chi-tiet-<%#Eval("khoi_id") %>/<%# cls_ToAscii.ToAscii(Eval("luyentap_name").ToString().ToLower()) %>-<%#Eval("test_id") %>" title="<%#Eval("luyentap_name") %>"><strong><%#Eval("luyentap_name") %></strong></a>
                                    <span class="icon_new"></span>
                                </h2>
                                <ul class="exam-info css-exam-info list-inline form-tooltip">
                                    <li class="pointer" data-toggle="tooltip" data-original-title="Số câu"><em class="fa fa-heart-o">&nbsp;</em><%#Eval("tongcauhoi") %><span class="hidden-xs"> câu hỏi</span></li>
                                    <li class="pointer" data-toggle="tooltip" data-original-title="Thời gian"><em class="fa fa-clock-o">&nbsp;</em>15<span class="hidden-xs"> phút</span></li>
                                </ul>
                                <ul class="list-inline help-block">
                                    <li><em class="fa fa-clock-o">&nbsp;</em>20:27 22/12/2021</li>
                                    <li><em class="fa fa-search">&nbsp;</em>Đã xem: 3</li>
                                </ul>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
                <%--    <asp:Repeater ID="rpTracNghiem" runat="server">
                        <ItemTemplate>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-12">
                                <div id="card" class="card card__tracnghiem">
                                    <div class="card-header card-header__test">
                                        <%#Eval("luyentap_name") %>
                                    </div>
                                    <div class="card-body card-body__test">
                                    </div>
                                    <div class="card-footer card-footer__test">
                                        <a href="/bai-luyen-tap-chi-tiet-<%#Eval("khoi_id") %>/<%# cls_ToAscii.ToAscii(Eval("luyentap_name").ToString()) %>-<%#Eval("test_id") %>.html" class="candy">Làm bài</a>
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>--%>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="hibelowbottom" runat="Server">
</asp:Content>
<asp:Content ID="Content11" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content12" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

