﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="web_MaTranDeThi_Detail_Version2.aspx.cs" Inherits="web_module_web_MaTranDeThi_Detail_Version2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- Meta -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="keywords" content="SITE KEYWORDS HERE" />
    <meta name="description" content="" />
    <meta name='copyright' content='' />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Trắc nghiệm Online</title>
    <link rel="icon" type="image/png" href="/images/logovn-2295.png" />
    <link rel="stylesheet" href="/css/bootstrap.min.css" />
    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="/css/font-awesome.min.css" />
    <script src="../admin_js/sweetalert.min.js"></script>
    <link href="../../css/pageTest.css" rel="stylesheet" />
    <style>
        .tracnghiem__heading {
            font-size: 30px;
            font-weight: 600;
            color: #F45C43;
            text-transform: uppercase;
            transition: linear 0.3s;
            text-align: center;
            margin-bottom: 27px;
            padding-top: 17px;
        }

        .block-dactacauhoi {
            width: 100%;
            height: auto;
            /*height: 100%;*/
            border: 1px solid gray;
            display: flex;
        }

            .block-dactacauhoi .chapter-name {
                width: 150px;
            }

            .block-dactacauhoi .item-dacta {
                float: left;
                height: 100%;
                border-right: 1px solid gray;
            }

            .block-dactacauhoi .lesson-group {
                display: flex;
            }

                .block-dactacauhoi .lesson-group .lesson-name {
                    width: 200px;
                    border: 1px solid gray;
                }

        td.content-dacta {
            width: 700px;
        }

        td.count-number-question {
            width: 30px;
            text-align: center;
        }

        .question-item .question {
            display: flex;
        }

        .content_image * {
            margin-bottom: 0 !important;
            line-height: 25px !important;
        }
    </style>
</head>


<body>
    <form id="form1" runat="server">
        <script>
            function printDiv(divName) {
                var printContents = document.getElementById(divName).innerHTML;
                var originalContents = document.body.innerHTML;
                document.body.innerHTML = printContents;
                window.print();
                document.body.innerHTML = originalContents;
            }
        </script>
        <asp:ScriptManager runat="server" />
        <%-- Start Khung ma trận đề--%>
        <div id="context-noidung">
            <div>
                MA TRẬN ĐỀ KIỂM TRA .........
            NĂM HỌC: ..........
            MÔN ...........
            </div>
            <div>
                KHUNG MA TRẬN ĐỀ
            <table class="format table-bordered" style="margin: auto; text-align: center">
                <tr>
                    <th rowspan="3">TT</th>
                    <th rowspan="3">Chương/chủ đề</th>
                    <th rowspan="3">Nội dung/đơn vị kiến thức</th>
                    <th colspan="8">Mức độ nhận thức</th>
                    <th rowspan="3">Tổng % điểm</th>
                </tr>
                <tr>
                    <th colspan="2">Nhận biết</th>
                    <th colspan="2">Thông hiểu</th>
                    <th colspan="2">Vận dụng</th>
                    <th colspan="2">Vận dung cao</th>
                </tr>
                <tr>
                    <th>TNKQ</th>
                    <th>TL</th>
                    <th>TNKQ</th>
                    <th>TL</th>
                    <th>TNKQ</th>
                    <th>TL</th>
                    <th>TNKQ</th>
                    <th>TL</th>
                </tr>
                <asp:Repeater ID="rpMaTranDeThi" runat="server" OnItemDataBound="rpMaTranDeThi_ItemDataBound">
                    <ItemTemplate>
                        <tr>
                            <td>1</td>
                            <td><%#Eval("chapter_name") %></td>
                            <td><%#Eval("lesson_name") %></td>
                            <asp:Repeater ID="rpMaTranChiTiet" runat="server">
                                <ItemTemplate>
                                    <td><%#Eval("matranchitiet_socau") %> câu<br />
                                        (<%#Eval("matranchitiet_diem") %>đ)<br />
                                        <%#Eval("matranchitiet_phantram") %>%</td>
                                </ItemTemplate>
                            </asp:Repeater>

                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
                <tr>
                    <td colspan="3">Tổng</td>
                    <td><%=sum_count_NB_TN%></td>
                    <td><%=sum_count_NB_TL%></td>
                    <td><%=sum_count_TH_TN%></td>
                    <td><%=sum_count_TH_TL%></td>
                    <td><%=sum_count_VD_TN%></td>
                    <td><%=sum_count_VD_TL%></td>
                    <td><%=sum_count_VDC_TN%></td>
                    <td><%=sum_count_VDC_TL%></td>
                    <td><%=(sum_count_NB_TN+sum_count_NB_TL+sum_count_TH_TN+sum_count_TH_TL+sum_count_VD_TN+sum_count_VD_TL+sum_count_VDC_TN+sum_count_VDC_TL)%></td>
                </tr>
                <tr>
                    <td colspan="3">Tỉ lệ %</td>
                    <td colspan="2"><%=tileNhanBiet%>%</td>
                    <td colspan="2"><%=tileThongHieu%>%</td>
                    <td colspan="2"><%=tileVanDung%>%</td>
                    <td colspan="2"><%=tileVanDungCao%>%</td>
                    <td><%=(tileVanDung+tileVanDungCao+tileNhanBiet+tileThongHieu) %>%</td>
                </tr>

                <tr>
                    <td colspan="3">Tỉ lệ chung</td>
                    <td colspan="4"><%=(tileNhanBiet+tileThongHieu) %>%</td>
                    <td colspan="4"><%=(tileVanDung+tileVanDungCao) %>%</td>
                    <td><%=(tileVanDung+tileVanDungCao+tileNhanBiet+tileThongHieu) %>%</td>
                </tr>
            </table>
            </div>
            <%-- End Khung ma trận đề--%>
            <%-- Start đặc tả ma trận đề--%>
            <div>
                BẢNG ĐẶC TẢ ĐỀ KIỂM TRA ...
            MÔN ...
            </div>
            <div>
                <table border="1" class="format" style="margin: auto; text-align: center">
                    <tr>
                        <td rowspan="2">STT</td>
                        <td rowspan="2">Chương/ Chủ đề</td>
                        <td rowspan="2">Nội dung/ Đơn vị kiến thức</td>
                        <td rowspan="2">Mức độ đánh giá</td>
                        <td colspan="4">Số câu hỏi theo mức độ nhận thức</td>
                    </tr>
                    <tr>
                        <td>Nhận biết</td>
                        <td>Thông hiểu</td>
                        <td>Vận dụng</td>
                        <td>Vận dụng cao</td>
                    </tr>
                </table>
                <asp:Repeater runat="server" ID="rpGroupChuong" OnItemDataBound="rpGroupChuong_ItemDataBound">
                    <ItemTemplate>
                        <div class="block-dactacauhoi">
                            <div class="item-dacta sothutu">
                                <%#Container.ItemIndex+1 %>
                            </div>
                            <div class="item-dacta chapter-name">
                                <%#Eval("chapter_name") %>
                            </div>
                            <div class="item-dacta">
                                <asp:Repeater runat="server" ID="rpLesson" OnItemDataBound="rpLesson_ItemDataBound">
                                    <ItemTemplate>
                                        <div class="lesson-group">
                                            <div class="lesson-name">
                                                <%#Eval("lesson_name") %>
                                            </div>
                                            <table border="1">
                                                <asp:Repeater runat="server" ID="rpListDacTa" OnItemDataBound="rpDacTa_ItemDataBound">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td class="content-dacta"><%#Eval("dacta_content") %></td>
                                                            <asp:Repeater ID="rpDetailDangCauHoi" runat="server">
                                                                <ItemTemplate>
                                                                    <td class="count-number-question"><%#Eval("socau_nhanbiet") %></td>
                                                                    <td class="count-number-question"><%#Eval("socau_thonghieu") %></td>
                                                                    <td class="count-number-question"><%#Eval("socau_vandung") %></td>
                                                                    <td class="count-number-question"><%#Eval("socau_vandungcao") %></td>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </table>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
            <%-- End đặc tả ma trận đề--%>
            <div class="container" style="display: block">
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <div id="div_BaiTap" runat="server">
                            <div class="tracnghiem__heading-box">
                                <div class="content__heading">
                                    <h3 class="tracnghiem__heading">Nội dung đề</h3>
                                </div>
                            </div>
                            <div>
                                <div class="testing m-bottom get_width" id="top">
                                    <div id="questionlist" data-exam-id="435">
                                        <asp:Repeater runat="server" ID="rpCauHoi" OnItemDataBound="rpCauHoiDetals_ItemDataBound">
                                            <ItemTemplate>
                                                <div class="question-box">
                                                    <div class="panel panel-default question-item">
                                                        <div class="panel-body">
                                                            <div class="m-bottom question ">
                                                                <strong class="text-red">Câu <%#Container.ItemIndex+1 %>:&nbsp;</strong><%#Eval("noidungcauhoi") %>
                                                            </div>
                                                            <div class="row answer">
                                                                <div class="flex">
                                                                    <asp:Repeater runat="server" ID="rpCauTraLoi">
                                                                        <ItemTemplate>
                                                                            <div class="answer-item col-xs-6  col-md-6 col-sm-12">
                                                                                <label class="radio_question">
                                                                                    <%#Eval("name_label") %>.&nbsp;<%#Eval("answer_content") %>
                                                                                </label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <asp:Repeater ID="rpBaiTapTuaLuan" runat="server">
                                    <ItemTemplate>
                                        <%#Eval("luyentap_baitaptuluan") %>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                        <a href="#" class="btn btn-success" onclick="printDiv('div_BaiTap')">In đề</a>
                        <a href="#" class="btn btn-success" onclick="Export2Doc('context-noidung');">Lưu file word</a>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <%-- End đề thi--%>
    </form>
    <script src="/js/jquery-3.5.1.min.js"></script>
    <script src="/js/jquery-migrate.min.js"></script>
    <!-- Popper JS-->
    <script src="/js/popper.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="/js/bootstrap.min.js"></script>
    <!-- Main JS-->
    <script src="/js/main.js"></script>
    <script>
        function Export2Doc(element, filename = 'MaTranDeThi') {

            var preHtml = "<html xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:w='urn:schemas-microsoft-com:office:word' xmlns='http://www.w3.org/TR/REC-html40'><head><meta charset='utf-8'><title>Export HTML To Doc</title></head><body>";
            var postHtml = "</body></html>";

            var html = preHtml + document.getElementById(element).innerHTML + postHtml;

            var blob = new Blob(['\ufeff', html], {
                type: 'application/msword'
            });

            var url = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(html);


            filename = filename ? filename + '.doc' : 'document.doc';


            var downloadLink = document.createElement("a");

            document.body.appendChild(downloadLink);

            if (navigator.msSaveOrOpenBlob) {
                navigator.msSaveOrOpenBlob(blob, filename);
            } else {

                downloadLink.href = url;
                downloadLink.download = filename;
                downloadLink.click();
            }

            document.body.removeChild(downloadLink);
        }
    </script>
</body>
</html>
